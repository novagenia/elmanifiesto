/**
 * Copyright 2011 NOVAGENIA INFORMATION TECHNOLOGIES. All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification, are
 * permitted provided that the conditions in the LICENSE file are met.
 */

package com.novagenia.jira.plugins.jql.function;

import static com.atlassian.jira.util.dbc.Assertions.notNull;

import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;

import com.atlassian.jira.JiraDataType;
import com.atlassian.jira.JiraDataTypes;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.link.IssueLink;
import com.atlassian.jira.issue.link.IssueLinkManager;
import com.atlassian.jira.issue.link.IssueLinkType;
import com.atlassian.jira.issue.link.IssueLinkTypeManager;
import com.atlassian.jira.jql.operand.QueryLiteral;
import com.atlassian.jira.jql.query.QueryCreationContext;
import com.atlassian.jira.plugin.jql.function.AbstractJqlFunction;
import com.atlassian.jira.util.MessageSet;
import com.atlassian.jira.util.MessageSetImpl;
import com.atlassian.query.clause.TerminalClause;
import com.atlassian.query.operand.FunctionOperand;
import com.opensymphony.user.User;

@SuppressWarnings("deprecation")
public class BaseLinkedIssuesJqlFunction extends AbstractJqlFunction {

	final Logger log = Logger.getLogger(BlockedIssuesJqlFunction.class);
	
	protected final IssueLinkTypeManager issueLinkTypeManager;
	protected final IssueLinkManager issueLinkManager;
	private String linkName = null;
	private boolean isOutward = true;

	public BaseLinkedIssuesJqlFunction(IssueLinkManager issueLinkManager,
    		IssueLinkTypeManager issueLinkTypeManager) {
		super();
		this.issueLinkTypeManager = issueLinkTypeManager;
		this.issueLinkManager = issueLinkManager;
	}

	@Override
	public int getMinimumNumberOfExpectedArguments() {
		return 1;
	}

	@Override
	public JiraDataType getDataType() {
		return JiraDataTypes.ISSUE;
	}

	public String getLinkName() {
		return linkName;
	}

	public void setLinkName(String linkName) {
		this.linkName = linkName;
	}

	@Override
	public MessageSet validate(User searcher, FunctionOperand operand, TerminalClause terminalClause) {
	    MessageSet messageSet = new MessageSetImpl();
	    if (!issueLinkManager.isLinkingEnabled())
	    {
			log.warn("Linking is not enabled!");
	        messageSet.addErrorMessage("Linking is not enabled!");
	        return messageSet;
	    }
	
	    final List<String> args = operand.getArgs();
	    if (args.isEmpty()) {
	        return messageSet;
	    }	    	
        String argLinkName = args.get(0);

    	log.debug("Validating link type name: " + argLinkName);
        Collection<IssueLinkType> linkTypes = getAllLinkTypesByName(argLinkName);
        if (linkTypes.isEmpty() || null == issueLinkManager.getIssueLinks(linkTypes.iterator().next().getId())) {
            messageSet.addErrorMessage(getI18n().getText("jira.jql.function.linked.issues.link.type.not.found", getFunctionName(), argLinkName));
        }
        else {
        	log.debug("Storing link type name: " + argLinkName + " as " + (isOutward? "outward.": "inward."));
        	linkName = argLinkName;
        }

	    return messageSet;
	}

	@Override
	public List<QueryLiteral> getValues(QueryCreationContext queryCreationContext, FunctionOperand operand, TerminalClause terminalClause) {
	    notNull("queryCreationContext", queryCreationContext);
	    final List<QueryLiteral> literals = new LinkedList<QueryLiteral>();
	
	    Collection<IssueLinkType> linkTypes = getAllLinkTypesByName(linkName);
	    if (
	    		linkTypes.isEmpty() ||
	    		null == issueLinkManager.getIssueLinks(linkTypes.iterator().next().getId())
	    	) {
	    	if (null == linkTypes || linkTypes.isEmpty()) {
	    		log.warn("No link types for link name: " + linkName);
	    	} else {
	    		log.warn("No links for link type: " + linkTypes.iterator().next().getName());        		
	    	}
	        linkTypes = issueLinkTypeManager.getIssueLinkTypes();
	        for (IssueLinkType type: linkTypes) {
	        	log.debug("IssueLinkType: " + type.getName() + " (" + type.getInward() + ", " + type.getOutward() + ")");
	        }
	    	
	        return null;
	    }
	    
	    Set<Issue> linkedIssues = new LinkedHashSet<Issue>();
	    for (IssueLinkType linkType : linkTypes)
	    {
	        List<Issue> issues = new LinkedList<Issue>();
	        Collection<IssueLink> links = issueLinkManager.getIssueLinks(linkType.getId());
	        for (IssueLink link: links) {
	        	if (isOutward) {
	        		issues.add(link.getSourceObject());
	        	}
	        	else {
	        		issues.add(link.getDestinationObject());
	        	}
	        		
	        }
	        
	        if (issues != null)
	        {
	            linkedIssues.addAll(issues);
	        }
	    }
	    for (Issue issue : linkedIssues)
	    {
	        literals.add(new QueryLiteral(operand, issue.getId()));
	    }
	
	    return literals;        
	}

	protected Collection<IssueLinkType> getAllLinkTypesByName(String aLinkName) {
		Collection<IssueLinkType> linkTypes = new LinkedList<IssueLinkType>(); 
	    linkTypes.addAll(issueLinkTypeManager.getIssueLinkTypesByOutwardDescription(aLinkName));
	    if (linkTypes.isEmpty())
	    {
	    	linkTypes.addAll(issueLinkTypeManager.getIssueLinkTypesByInwardDescription(aLinkName));
	    	log.debug("Returning inward links");
	    	isOutward = false;
	    } else {
	    	log.debug("Returning outward links");
	    	isOutward = true;
	    }
		return linkTypes;
	}
}