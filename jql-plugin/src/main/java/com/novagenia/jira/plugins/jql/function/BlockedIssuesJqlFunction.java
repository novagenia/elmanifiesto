/**
 * Copyright 2011 NOVAGENIA INFORMATION TECHNOLOGIES. All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification, are
 * permitted provided that the conditions in the LICENSE file are met.
 */

package com.novagenia.jira.plugins.jql.function;

import org.apache.log4j.Logger;

import com.atlassian.jira.issue.link.IssueLinkManager;
import com.atlassian.jira.issue.link.IssueLinkTypeManager;
import com.atlassian.jira.util.MessageSet;
import com.atlassian.query.clause.TerminalClause;
import com.atlassian.query.operand.FunctionOperand;
import com.opensymphony.user.User;

@SuppressWarnings("deprecation")
public class BlockedIssuesJqlFunction extends BaseLinkedIssuesJqlFunction {

    final Logger log = Logger.getLogger(BlockedIssuesJqlFunction.class);

    final static String LINK_NAME = "depende de";

    public BlockedIssuesJqlFunction(IssueLinkManager issueLinkManager, IssueLinkTypeManager issueLinkTypeManager) {
        super(issueLinkManager, issueLinkTypeManager);
    }

    @Override
    public int getMinimumNumberOfExpectedArguments() {
        return 0;
    }

    @Override
    public MessageSet validate(User searcher, FunctionOperand operand, TerminalClause terminalClause) {
        MessageSet messages = super.validate(searcher, operand, terminalClause);

        if (!messages.hasAnyMessages()) {
            setLinkName(LINK_NAME);
        }

        return messages;
    }
}
